from flask_restful.reqparse import Namespace
from utils import _assign_if_something
from db import db

class CategoryModel(db.Model):
    __tablename__ = 'category'
    category_id = db.Column(db.BigInteger,primary_key = True)
    description = db.Column(db.String)

    def __init__(self, category_id, description):
        self.category_id = category_id
        self.description = description
    
    def json(self, jsondepth = 0):

        json = {
            'category_id': self.category_id,
            'description': self.description,
        }
            
        return json

    @classmethod
    def get_all(cls):
        return cls.query.all()
        
    @classmethod   
    def get_category(cls, category_id):
        return cls.query.filter_by(category_id = category_id).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
    
    def from_reqparse(self, newdata: Namespace):
        for no_pk_key in ['description']:
            _assign_if_something(self, newdata, no_pk_key)