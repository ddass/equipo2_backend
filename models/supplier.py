from flask_restful.reqparse import Namespace
from utils import _assign_if_something
from db import db

class SupplierModel(db.Model):
    __tablename__ = 'supplier'
    supplier_id = db.Column(db.BigInteger, primary_key = True)
    supplier_name = db.Column(db.String)
    supplier_phone = db.Column(db.String)
    supplier_address = db.Column(db.String)

    def __init__(self, supplier_id, supplier_name, supplier_phone, supplier_address):
        self.supplier_id = supplier_id
        self.supplier_name = supplier_name
        self.supplier_phone = supplier_phone
        self.supplier_address = supplier_address
    
    def json(self, jsondepth = 0):

        json = {
            'supplier_id': self.supplier_id,
            'supplier_name': self.supplier_name,
            'supplier_phone':self.supplier_phone,
            'supplier_address':self.supplier_address
        }
            
        return json
    @classmethod
    def get_all(cls):
        return cls.query.all()
        
    @classmethod  
    def get_supplier(cls, supplier_id):
        return cls.query.filter_by(supplier_id = supplier_id).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
    
    def from_reqparse(self, newdata: Namespace):
        for no_pk_key in ['supplier_name','supplier_phone','supplier_address']:
            _assign_if_something(self, newdata, no_pk_key)