from models.category import CategoryModel
from models.supplier import SupplierModel
from utils import _assign_if_something
from flask_restful.reqparse import Namespace
from db import db

class ProductModel(db.Model):
    __tablename__ = 'products'

    product_id = db.Column(db.BigInteger, primary_key=True)
    description = db.Column(db.String)
    status = db.Column(db.String)
    product_name = db.Column(db.String)
    price = db.Column(db.BigInteger)
    supplier_id = db.Column(db.BigInteger, db.ForeignKey(SupplierModel.supplier_id))
    category_id =db.Column(db.BigInteger, db.ForeignKey(CategoryModel.category_id))

    _supplier = db.relationship('SupplierModel', uselist=False, primaryjoin='SupplierModel.supplier_id == ProductModel.supplier_id', foreign_keys='ProductModel.supplier_id')
    _category = db.relationship('CategoryModel', uselist=False, primaryjoin='CategoryModel.category_id == ProductModel.category_id', foreign_keys='ProductModel.category_id')

    def __init__(self,product_id,description, status,product_name,price, supplier_id, category_id):
        self.product_id = product_id
        self.description = description
        self.status = status
        self.product_name = product_name
        self.price = price
        self.supplier_id = supplier_id
        self.category_id = category_id

    def json(self, jsondepth = 1):

        json = {
            'product_id': self.product_id,
            'description': self.description,
            'status': self.status,
            'price':self.price,
            'product_name':self.product_name,
            'supplier_id': self.supplier_id,
            'category_id':self.category_id
        }
        
        if jsondepth > 0:
            if self._supplier:
                json['_supplier'] = self._supplier.json(jsondepth)
            if self._category:
                json['_category'] = self._category.json(jsondepth)
            
        return json

    @classmethod
    def get_all(cls):
        return cls.query.all()

    @classmethod
    def get_product(cls, product_id):
        return cls.query.filter_by(product_id = product_id).first()
    
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
    
    def from_reqparse(self, newdata: Namespace):
        for no_pk_key in ['description','status','price','product_name','supplier_id','category_id']:
            _assign_if_something(self, newdata, no_pk_key)

    
    