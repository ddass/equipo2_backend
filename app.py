import logging
import os
from resources.product import *
from resources.category import *
from resources.supplier import *
from flask import Flask, redirect, jsonify
from flask_restful import Api
from flask_cors import CORS
from flasgger import Swagger
from db import db
from flask_cors import CORS

app = Flask(__name__)

api = Api(app, errors={
    'NoAuthorizationError': {
        "message": "La respueta no tiene token",
        "error": "authorization_required",
        "status": 401
    }
})

PREFIX = os.environ.get('PREFIX_PATH','/api')

# Swagger config
app.config['SWAGGER'] = {
    'title': 'equipo2-backend',
    'version': '1.0.0',
    'description': 'APIs para proyecto backend grupal en Flask ',
    'uiversion': 2,
    'tags': [{'name': 'jwt'}],
    'specs': [{
        'endpoint': 'apispec_1',
        'route': f'{PREFIX}/apispec_1.json',
        'rule_filter': lambda rule: True,  # all in
        'model_filter': lambda tag: True  # all in
    }],
    'specs_route': f"{PREFIX}/apidocs/",
    'static_url_path': f'{PREFIX}/static'
}

swagger = Swagger(app)

app.logger.setLevel(logging.INFO)

def env_config(name, default):
    app.config[name] = os.environ.get(name, default = default)

# Database config
env_config('SQLALCHEMY_DATABASE_URI', 'postgresql://postgres:postgres@localhost:5432/equipo2backend')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['PROPAGATE_EXCEPTIONS'] = True
app.config['SQLALCHEMY_ECHO'] = True

# Enable CORS
CORS(app,resources={r'/*':{'origins': '*'}})


@app.route("/")
@app.route(f'{PREFIX}')
def welcome():
    return redirect(f"{PREFIX}/apidocs", code= 302)



# URIs
api.add_resource(Product, f'{PREFIX}/products/<id>')
api.add_resource(ProductList, f'{PREFIX}/products')
api.add_resource(ProductSearch, f'{PREFIX}/search/products')
api.add_resource(Category, f'{PREFIX}/categories/<id>')
api.add_resource(CategoryList, f'{PREFIX}/categories')
api.add_resource(CategorySearch, f'{PREFIX}/search/categories')
api.add_resource(Supplier,f'{PREFIX}/suppliers/<id>')
api.add_resource(SupplierList, f'{PREFIX}/suppliers')
api.add_resource(SupplierSearch, f'{PREFIX}/search/suppliers')

if __name__ == '__main__':
    db.init_app(app)
    app.run(debug=True)
else: 
    db.init_app(app)
