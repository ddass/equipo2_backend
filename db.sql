--------------------------------------------------
--Script para generar la base de datos--
--------------------------------------------------

drop table if exists products;
drop table if exists category;
drop table if exists supplier;



create table supplier(
	supplier_id serial,
	supplier_name varchar(255) not null,
	supplier_phone varchar(255) not null,
	supplier_address varchar(255) not null,
	primary key (supplier_id)

);
create table category (
	category_id serial,
	description varchar(255) not null,
	primary key (category_id)
);

create table products (
	product_id serial,
	description varchar(255) not null,
	product_name varchar(255) not null, 
	price int not null,
	status varchar(255) not null,
	supplier_id int not null,
	category_id int not null,
	primary key (product_id),
	constraint fk_supplier
		foreign key (supplier_id)
			references supplier(supplier_id),
	constraint fk_category
		foreign key (category_id)
			references category(category_id)		

);
