import logging
from warnings import filters
from flasgger import swag_from
from flask import request
from yaml import parse
from models.supplier import SupplierModel
from flask_restful import Resource, reqparse
from utils import paginated_results, restrict

class Supplier(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('supplier_id',type = int)
    parser.add_argument('supplier_name',type = str)
    parser.add_argument('supplier_phone',type = str)
    parser.add_argument('supplier_address',type = str)


    @swag_from('../swagger/supplier/get_supplier.yaml')
    def get(self,id):
        supplier = SupplierModel.get_supplier(id)
        if supplier:
            return supplier.json()
        return {'message':'No se encuentra el proveedor'},404

    @swag_from('../swagger/supplier/put_supplier.yaml')
    def put(self,id):
        supplier = SupplierModel.get_supplier(id)
        if supplier:
            newdata = Supplier.parser.parse_args()
            supplier.from_reqparse(newdata)
            supplier.save_to_db()
            return supplier.json()
        return {'message':'No se encuentra el proveedor'},404

    @swag_from('../swagger/supplier/delete_supplier.yaml')
    def delete(self,id):
        supplier = SupplierModel.get_supplier(id)
        if supplier:
            supplier.delete_from_db()
        return {'message':'Proveedor borrado'}

class SupplierList(Resource):

    @swag_from('../swagger/supplier/list_suppliers.yaml')
    def get(self):
        query = SupplierModel.query
        return paginated_results(query)
    
    @swag_from('../swagger/supplier/post_supplier.yaml')
    def post(self):

        data = Supplier.parser.parse_args()
        id = data.get('id')

        if id is not None and SupplierModel.get_supplier(id):
            return {'message': "Ya existe un proveedor con el id"}, 404
        
        supplier = SupplierModel(**data)
        try:
            supplier.save_to_db()
        except Exception as e:
            logging.error('Ocurrio un error con el proveedor',exc_info=e)
            return {'message':'Ocurrio un error con el proveedor'}, 404
        return supplier.json(), 201

class SupplierSearch(Resource):

    @swag_from('../swagger/supplier/search_supplier.yaml')
    def post (self):
        query = SupplierModel.query
        if request.json:
            filters = request.json
            query = restrict (query, filters,'supplier_id', lambda x: SupplierModel.supplier_id == x)
            query = restrict (query, filters, 'supplier_name',lambda x: SupplierModel.supplier_name.contains(x))
            query = restrict (query, filters, 'supplier_phone',lambda x: SupplierModel.supplier_phone.contains(x))
            query = restrict (query, filters, 'supplier_address',lambda x: SupplierModel.supplier_address.contains(x))
        return paginated_results(query)


    

