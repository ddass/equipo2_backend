import logging
from warnings import filters
from flasgger import swag_from
from flask import request
from models.category import CategoryModel
from flask_restful import Resource, reqparse
from utils import paginated_results, restrict

class Category(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('category_id',type = int)
    parser.add_argument('description',type = str)

    @swag_from('../swagger/category/get_category.yaml')
    def get(self,id):
        category = CategoryModel.get_category(id)
        if category:
            return category.json()
        return {'message':'No se encuentra la categoria'},404

    @swag_from('../swagger/category/put_category.yaml')
    def put(self,id):
        category = CategoryModel.get_category(id)
        if category:
            newdata = Category.parser.parse_args()
            category.from_reqparse(newdata)
            category.save_to_db()
            return category.json()
        return {'message':'No se encuentra la categoria'},404

    @swag_from('../swagger/category/delete_category.yaml')
    def delete(self,id):
        category = CategoryModel.get_category(id)
        if category:
            category.delete_from_db()
        return {'message':'Categoria borrada'}

class CategoryList(Resource):

    @swag_from('../swagger/category/list_categories.yaml')
    def get(self):
        query = CategoryModel.query
        return paginated_results(query)
    
    @swag_from('../swagger/category/post_category.yaml')
    def post(self):

        data = Category.parser.parse_args()
        id = data.get('id')

        if id is not None and CategoryModel.get_category(id):
            return {'message': "Ya existe una categoria con el id"}, 404
        
        category = CategoryModel(**data)
        try:
            category.save_to_db()
        except Exception as e:
            logging.error('Ocurrio un error con la categoria',exc_info=e)
            return {'message':'Ocurrio un error con la categoria'}, 404
        return category.json(), 201

class CategorySearch(Resource):

    @swag_from('../swagger/category/search_category.yaml')
    def post (self):
        query = CategoryModel.query
        if request.json:
            filters = request.json
            query = restrict (query, filters,'category_id', lambda x: CategoryModel.category_id == x)
            query = restrict (query, filters,'description', lambda x: CategoryModel.description.contains(x))
        return paginated_results(query)


    

