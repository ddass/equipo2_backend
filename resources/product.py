import logging
from warnings import filters
from flasgger import swag_from
from flask import request
from yaml import parse
from models.product import ProductModel
from flask_restful import Resource, reqparse
from utils import paginated_results, restrict

class Product(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('product_id',type = int)
    parser.add_argument('description',type = str)
    parser.add_argument('product_name',type = str)
    parser.add_argument('price',type = int)
    parser.add_argument('status',type = str)
    parser.add_argument('supplier_id',type = int)
    parser.add_argument('category_id',type = int)


    @swag_from('../swagger/product/get_product.yaml')
    def get(self,id):
        product = ProductModel.get_product(id)
        if product:
            return product.json()
        return {'message':'No se encuentra el producto'},404

    @swag_from('../swagger/product/put_product.yaml')
    def put(self,id):
        product = ProductModel.get_product(id)
        if product:
            newdata = Product.parser.parse_args()
            product.from_reqparse(newdata)
            product.save_to_db()
            return product.json()
        return {'message':'No se encuentra el producto'},404

    @swag_from('../swagger/product/delete_product.yaml')
    def delete(self,id):
        product = ProductModel.get_product(id)
        if product:
            product.delete_from_db()
        return {'message':'Producto borrado'}

class ProductList(Resource):

    @swag_from('../swagger/product/list_products.yaml')
    def get(self):
        query = ProductModel.query
        return paginated_results(query)
    
    @swag_from('../swagger/product/post_product.yaml')
    def post(self):

        data = Product.parser.parse_args()
        id = data.get('id')

        if id is not None and ProductModel.get_product(id):
            return {'message': "Ya existe un producto con el id"}, 404
        
        product = ProductModel(**data)
        try:
            product.save_to_db()
        except Exception as e:
            logging.error('Ocurrio un error con el producto',exc_info=e)
            return {'message':'Ocurrio un error con el producto'}, 404
        return product.json(),201

class ProductSearch(Resource):

    @swag_from('../swagger/product/search_product.yaml')
    def post (self):
        query = ProductModel.query
        if request.json:
            filters = request.json
            query = restrict (query, filters,'product_id', lambda x: ProductModel.product_id == x)
            query = restrict (query, filters,'description', lambda x: ProductModel.description.contains(x))
            query = restrict (query, filters,'price', lambda x: ProductModel.price == x)
            query = restrict (query, filters, 'product_name',lambda x: ProductModel.product_name.contains(x))
            query = restrict (query, filters, 'status',lambda x: ProductModel.status.contains(x))
            query = restrict (query, filters,'category_id', lambda x: ProductModel.category_id == x)
            query = restrict (query, filters,'supplier_id', lambda x: ProductModel.supplier_id == x)
        return paginated_results(query)


    

