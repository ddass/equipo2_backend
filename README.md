# Proyecto Backend en Python/Flask
Esto es un proyecto backend para el modulo de "Metodos de Gestion de Software"
# Flujo de trabajo
Se trabaja a partir de la rama main.
Cada integrante del grupo tiene una rama con su nombre en la cual trabaja.
**Cuando termine, pedir un merge request a main**
## Equipo-2
- Adriana Frutos
- Agustin Arce
- Andrea Riveros
# Leer el TODO.md

## Dependencias

- Python 3.6+

## Set up
Preparar un entorno virtual nuevo a fin de resolver las dependencias en el mismo
 - ### Opcion 1
    Crear entorno
    ```bash
    python -m venv [nombre entorno]
    ```
    Activar
    ```bash
    . [nombre entorno]/bin/activate
    ```
- ### Opcion 2
    Crear entorno
    ```bash
    $ virtualenv venv
    ```
    Activar
    ```bash
    . [nombre entorno]/bin/activate
    ```

Resolver las dependencias de python usando el arhivo `requirements.txt`

```bash
$ pip install -r requirements.txt
```

## Uso

Para iniciar el servidor edite las opciones en `app.py` y luego ejecútelo

```bash
$ python app.py
```
o
```bash
$ flask run
```

